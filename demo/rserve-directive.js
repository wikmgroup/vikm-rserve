/************************ LICENCE ***************************
*     This file is part of <ViKM Vital-IT Knowledge Management web application>
*     Copyright (C) <2016> SIB Swiss Institute of Bioinformatics
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU Affero General Public License as
*     published by the Free Software Foundation, either version 3 of the
*     License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Affero General Public License for more details.
*
*     You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>
*
*****************************************************************/


/*global angular*/
(function(){
	'use strict';

	/**
	* @ngdoc directive
	* @name vikmApp.directive:compareTo
	* @description used to compare to models. Used initially to compare password and confirm password
	* # compareTo
	*/
	angular.module('vikmApp')
	.directive('rserveDemo',rserveDemo);

	rserveDemo.$inject = ['Rserve'];
	function rserveDemo(Rserve){
		return{
			scope: {
				filepath: '='
			},
			template: '<p ng-if = "rows.length===0"><button class = "btn btn-default" ng-click = "RStart()">get data</button></p> \
				<div ng-if = "rows.length"> \
					<p>Number of rows to display: <select name = "nbRows" ng-model = "nbRows" ng-change = "RGetData(this.nbRows)"><option value = "5">5</option><option value = "10">10</option><option value = "15">15</option><option value = "20">20</option></select></p> \
					<table class = "table"> \
						<tr> \
						 <th ng-repeat = "th in headers">{{th}}</th> \
						</tr> \
						<tr ng-repeat = "tr in rows"> \
							<td ng-repeat = "td in tr">{{td}}</td> \
						</tr> \
					</table> \
				</div> \
			',
			link: function(scope){
				scope.RStart = RStart;
				scope.RGetData = RGetData;
				scope.nbRows = "5";
				scope.rows = [];
				scope.headers = [];
				function RStart(){
					Rserve.start().then(function(){
						Rserve.loadData(scope.filepath).then(function(){
							RGetData(5);
						});

					});
				}

				function RGetData(nbRows){
					Rserve.Rdemo(nbRows).then(function successCallback(data) {
						scope.rows = data.data.data;
						scope.headers = data.data.headers;
					}, function errorCallback(errorMsg) {
						toastr(errorMsg);
					});
				}
			}
		}
	}
})();
