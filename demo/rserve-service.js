/************************ LICENCE ***************************
*     This file is part of <ViKM Vital-IT Knowledge Management web application>
*     Copyright (C) <2016> SIB Swiss Institute of Bioinformatics
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU Affero General Public License as
*     published by the Free Software Foundation, either version 3 of the
*     License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Affero General Public License for more details.
*
*     You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>
*
*****************************************************************/


/*global angular*/
(function(){
	'use strict';

	/**
	* @ngdoc service
	* @name targetomeApp.Rserve
	* @description
	* # Rserve
	* Factory in the targetomeApp.
	*/
	angular.module('vikmApp')
	.factory('Rserve',RserveFactory);

	RserveFactory.$inject = ['$http','ENV','$interval','toastr'];
	function RserveFactory($http,ENV,$interval,toastr){
		var service = {
			socketeerID: '',
			start: start,
			keepAlive: keepAlive,
			loadData: loadData,
			Rdemo: Rdemo
		};
		var serverRoot = ENV.serverURL.split('api')[0]+"R/";


		// function to start a new connection to RServe. Register a new socket with socketeer and get it's ID. It will then keeps the socket alive with $interval call.
		function start(){

			return $http.get(serverRoot+"newskt").then(function successCallback(data) {

				service.socketeerID = data.data;
				$interval(function(){
					keepAlive();
				},20000)
			}, function errorCallback(data,status) {
				service.socketeerID = '';
				toastr.error("No connection to RServe");

			});

		}

		// function to keep the socket alive. Otherwise, the socket dies after 120 seconds (default)

		function keepAlive(){
			if(!service.socketeerID) start();
			return $http.get(serverRoot+service.socketeerID);
		}


		// function to load the data in the R session.
		function loadData(dataPath){
			if(!service.socketeerID) toastr.error("No connection to RServe");
			else{
				return $http.get(serverRoot+"/"+service.socketeerID+"/load_data?file="+dataPath);
			}
		}

		// example of an R function to query a dataset
		function Rdemo(nb){
			if(!service.socketeerID) toastr.error("No connection to RServe");
			else{
				var url = serverRoot+"/"+service.socketeerID + "/Rdemo";
				if(nb) url += "?nb="+nb;
				return $http.get(url);
			}
		}

		return service;
	}
})();