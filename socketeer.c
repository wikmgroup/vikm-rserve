/************************ LICENCE ***************************
*     This file is part of <ViKM Vital-IT Knowledge Management web application>
*     Copyright (C) <2016> SIB Swiss Institute of Bioinformatics
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU Affero General Public License as
*     published by the Free Software Foundation, either version 3 of the
*     License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Affero General Public License for more details.
*
*     You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>
*
*****************************************************************/


/*******************************************************************************
 * socketeer.c -- A socket manager for 'Rserve' connections
 *
 * This program creates a kind of socket-tunnel between a global 'Rserve' daemon
 * socket, and a local persistant named Unix socket.
 *
 * Usage: socketeer
 *
 * Description:
 *   When interfacing Apache/PHP pages with a daemonized 'Rserve' server,
 * the child 'Rserve' process dies as soon as the PHP web-page is displayed,
 * thus loosing all the benefits from having an independent 'Rserve' child.
 * The working logic behind 'socketeer', is to create a named Unix socket,
 * containing a unique identifier (like a SessionID) and make a tunnel between
 * this socket and the 'Rserve' socket. Thus keeping the individual 'Rserve'
 * instances alive and separate.
 *
 * Author:      Volker Flegel
 *
 * Date:  17.06.2013
 *
 * History:
 *   - 12.10.2016: Added MacOS specific socket code.
 *                 Rewrite select() timeout handling, as the timeval struct
 *                 is not modified in every POSIX OSes.
 *   - 22.07.2013: Correct send() error handling (do not enter partial send loop)
 *   - 27.06.2013: First Release Candidate.
 *   - 17.06.2013: Start of code.
 *
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>


#define BUFSIZE  65536  // size of in/out buffer (OK: 65536)
#define ERR_STR  32     // size of date error string
#define SRNDSIZE 16     // size of random ID in socket name
#define TO_SEC   120     // Socket timeout (seconds)
#define TO_USEC  0      // Socket timeout (micro-seconds)

#define GET_TIME e_time = time(NULL); strftime(s_error, 1024, "%b %d %Y %H:%M:%S", localtime(&e_time));

/* Rserve socket name - must match the name used when starting Rserve */
#ifndef RSERVE_SOCK
/*#define RSERVE_SOCK "/var/run/rserve/Rserve.sock"*/
#define RSERVE_SOCK "/Library/WebServer/Documents/vikm/vikmapp-backend/tools/run/Rserve.sock"
#endif

/* socket name -  */
#ifndef LISTEN_SOCK
#define LISTEN_SOCK "/tmp/socketeer."
#endif

/* Logfile name */
#ifndef LOG_NAME
#define LOG_NAME "/Library/WebServer/Documents/vikm/vikmapp-backend/log/socketeer.log"
#endif
//const char *sock = RSERVE_SOCK;

// Global variables
char s_name[sizeof(LISTEN_SOCK) + SRNDSIZE + 1];
char s_error[ERR_STR];  // Store timestamp for error logging
pid_t mypid;            // Store PID
time_t e_time;          // Store time values for logging
time_t s_time;          // Store process start time
int nb_clients = 0;
int tt_clients = 0;
unsigned long tt_bytes = 0;

// Function definitions
void clean_exit();

/*******************************************************************************
 * gen_random(): Generate a string with random digits
 ******************************************************************************/
void gen_random(char *s, const int len) {
  static const char alphanum[] = "0123456789";
  int i,k;

  for (i = 0; i < len; ++i) {
    k = rand() / (RAND_MAX / (sizeof(alphanum) - 1));
    s[i] = alphanum[k];
  }
  s[len] = 0;
}

/*******************************************************************************
 * c_accept(): Accept a new connection on our listening socket
 ******************************************************************************/
/* Returns:
 *   0 - No new connection
 *   1 - New connection accepted
 ******************************************************************************/
int c_accept(int in_fd, fd_set *master, fd_set *client_fds, int *fdmax) {
  int n_fd;                         // Newly accept()ed socket filehandle
  struct sockaddr_un remote;
  socklen_t slen = sizeof(remote);  // Store sockaddr length

  n_fd = accept(in_fd, (struct sockaddr *)&remote, &slen);
  if (n_fd == -1) {
    GET_TIME
    fprintf(stderr, "%s %d - WARN - Error on accept(): %s\n", s_error, mypid, strerror(errno));
    return 0;
  } else {
    FD_SET(n_fd, master);      // Add to master set
    FD_SET(n_fd, client_fds);  // Add as a rserve message client
    if (n_fd > *fdmax) {       // Set max FD
      *fdmax = n_fd;
    }
    //GET_TIME
    //fprintf(stderr, "%s %d - INFO - New connection on socket %d\n", s_error, mypid, n_fd);
  }
return 1;
}




/*******************************************************************************
 * sig_clean_exit(): Do a clean exit (at least unlink() the socket file)
 ******************************************************************************/
void sig_clean_exit(int signo) {
  GET_TIME
  fprintf(stderr,"%s %d - INFO - Received signal. Exiting...\n", s_error, mypid);
  clean_exit();
}


/*******************************************************************************
 * main(): Do the processing
 ******************************************************************************/
int main(void)
{
  mypid = getpid();   // Store my PID for further use
  fd_set master;      // master file descriptor list
  fd_set read_fds;    // temp file descriptor list for select()
  fd_set client_fds;  // File descriptor set of client connections (no Rserve FD)
  int fdmax;          // maximum file descriptor number

  int l_fd;           // listening socket filehandle
  int r_fd;           // Rserve socket filehandle

  int nbytes;
  int i, j;

  s_time=time(NULL);
  // Allocate communication buffer
  char * buf = malloc(BUFSIZE * sizeof(char));
  if (buf == NULL) {
    GET_TIME
    fprintf(stderr, "%s %d - ERROR - Buffer allocation error in malloc(): %s\n", s_error, mypid, strerror(errno));
    exit(1);
  }
  // Initialize our socket name
  strcpy(s_name, LISTEN_SOCK);

  // Initialize random number generator
  srand(time(NULL) + mypid);
  // Create a socket number (may not be the best solution)
  char random[SRNDSIZE + 1];
  gen_random(random, SRNDSIZE);
  strcat(s_name, random);
  /*GET_TIME
  fprintf(stderr, "%s %d - INFO - random(%d): %s, %s\n", s_error, mypid, SRNDSIZE, random, s_name);*/

  // Declare socket variables
  struct sockaddr_un local, rserve;  // local listening and rserve socket
  int len;                           // store sockaddr length

  // Set a timeout for our listening socket
  struct timeval tv;

  // clear the master, temp sets and client sets
  FD_ZERO(&master);
  FD_ZERO(&read_fds);
  FD_ZERO(&client_fds);

  // Open 'Rserve' main socket
  r_fd = socket(AF_UNIX, SOCK_STREAM, 0);
  if (r_fd < 0) {
    GET_TIME
    fprintf(stderr, "%s %d - ERROR - Unable to create socket(rserve): %s\n", s_error, mypid, strerror(errno));
    exit(1);
  }
  // Connecting to 'Rserve' socket
  rserve.sun_family = AF_UNIX;
  strcpy(rserve.sun_path, RSERVE_SOCK);
  len= strlen(rserve.sun_path) + sizeof(rserve.sun_family);
  #ifdef __APPLE__
  if (connect(r_fd, (struct sockaddr *)&rserve, SUN_LEN(&rserve)) == -1) {
  #else
  if (connect(r_fd, (struct sockaddr *)&rserve, len) == -1) {
  #endif
    GET_TIME
    fprintf(stderr, "%s %d - ERROR - Unable to connect() socket rserve: %s\n", s_error, mypid, strerror(errno));
    exit(1);
  }
  // Keep track of the biggest file descriptor
  fdmax = r_fd; // so far, it's this one

  // Open own listener socket
  l_fd = socket(AF_UNIX, SOCK_STREAM, 0);
  if (l_fd < 0) {
    GET_TIME
    fprintf(stderr, "%s %d - ERROR - Unable to create socket(socketeer): %s\n", s_error, mypid, strerror(errno));
    exit(1);
  }
  // Bind to own listener socket
  local.sun_family = AF_UNIX;
  strcpy(local.sun_path, s_name);
  unlink(local.sun_path);
  len = strlen(local.sun_path) + sizeof(local.sun_family);
  #ifdef __APPLE__
  if (bind(l_fd, (struct sockaddr *)&local, SUN_LEN(&local)) == -1) {
  #else
  if (bind(l_fd, (struct sockaddr *)&local, len) < 0) {
  #endif
    GET_TIME
    fprintf(stderr, "%s %d - ERROR - Unable to bind() to '%s': %s\n", s_error, mypid, s_name, strerror(errno));
    exit(2);
  }

  // listen on the socket with a backlog arbitrarily set to 2
  if (listen(l_fd, 2) == -1) {
    GET_TIME
    fprintf(stderr, "%s %d - ERROR - Unable to listen() on '%s': %s\n", s_error, mypid, s_name, strerror(errno));
    exit(3);
  }
  // Track largest FD
  if (l_fd > fdmax) { fdmax = l_fd; }
  // Add the listener to the master set (do not add rserve yet, we wait till a client connects)
  FD_SET(l_fd, &master);

  //
  // Ready to fork() and background
  //
  pid_t cpid;
  if ((cpid = fork()) < 0) {
    GET_TIME
    fprintf(stderr, "%s %d - ERROR - Unable to fork(): %s\n", s_error, mypid, strerror(errno));
    unlink(s_name);
    exit(4);
  } else if (cpid > 0) {
    // Output socket name and exit gracefully
    fprintf(stdout, "%s\n", random);
    exit(0);
    // END: of calling process
  }

  // We are a child process, get the new PID
  mypid = getpid();
  // Close/re-open stdin, out and err and change to unbuffered output
  if (freopen("/dev/null", "r", stdin) == NULL) {
    GET_TIME
    fprintf(stderr, "%s %d - ERROR - Unable to reopen stdin: %s\n", s_error, mypid, strerror(errno));
    exit(4);
  }
  if (freopen(LOG_NAME, "a", stdout) == NULL) {
    GET_TIME
    fprintf(stderr, "%s %d - WARN - Disabling logging: %s - %s\n", s_error, mypid, LOG_NAME, strerror(errno));
    if (freopen("/dev/null", "a", stdout) == NULL)
      exit(4);
  }
  if ((freopen(LOG_NAME, "a", stderr) == NULL) && (freopen("/dev/null", "a", stderr) == NULL))
      exit(4);
  if (setvbuf(stdout, NULL, _IONBF, 0) != 0) {
    GET_TIME
    fprintf(stderr, "%s %d - WARN - Unable to setvbuf() stdout: %s\n", s_error, mypid, strerror(errno));
  }
  if (setvbuf(stderr, NULL, _IONBF, 0) != 0) {
    GET_TIME
    fprintf(stderr, "%s %d - WARN - Unable to setvbuf() stderr: %s\n", s_error, mypid, strerror(errno));
  }

  // Trap some signals
  if (signal(SIGINT, sig_clean_exit) == SIG_ERR) {
    GET_TIME
    fprintf(stderr, "%s %d - WARN - Unable to trap SIGINT: %s\n", s_error, mypid, strerror(errno));
  }
  if (signal(SIGTERM, sig_clean_exit) == SIG_ERR) {
    GET_TIME
    fprintf(stderr, "%s %d - WARN - Unable to trap SIGTERM: %s\n", s_error, mypid, strerror(errno));
  }

  //
  // Main loop
  //
  GET_TIME
  fprintf(stderr, "%s %d - INFO - Started new socketeer - PID: %d - SocketID: %s\n", s_error, mypid, mypid, random);

  for(;;) {
    read_fds = master;      // Set the read FD set to the master FD set
    tv.tv_sec = TO_SEC;     // Initialize TimeOut for 'select()' operation
    tv.tv_usec = TO_USEC;

    // Listen on all our sockets for activity, exit on timeout
    int ret = select(fdmax+1, &read_fds, NULL, NULL, &tv);
    if (ret == -1) {
      GET_TIME
      fprintf(stderr, "%s %d - ERROR - select() returned error: %s\n", s_error, mypid, strerror(errno));
      unlink(s_name);
      exit(4);
    } else if (ret == 0) {
      // If timeout, we exit
      GET_TIME
      fprintf(stderr, "%s %d - INFO - Inactivity timeout - PID: %d - SocketID: %s. Exiting...\n", s_error, mypid, mypid, random);
      clean_exit();
    }

    // Run through the existing connections looking for data to read
    for (i = 0; i <= fdmax; i++) {
      if (FD_ISSET(i, &read_fds)) { // New incomming data
        if (i == l_fd) {
          //
          // New connection on listener socket
          nb_clients += c_accept(l_fd, &master, &client_fds, &fdmax);
          tt_clients++;
          //GET_TIME
          //fprintf(stderr, "%s %d - INFO - Have %d client(s).\n", s_error, mypid, nb_clients);
          // Add Rserve socket to read set if we have a client
          if (nb_clients > 0) { FD_SET(r_fd, &master); }
        } else {
          //
          // Get data from a client and handle disconnects
          if ((nbytes = recv(i, buf, BUFSIZE, 0)) <= 0) {
            // Got error or connection closed by client
            if (nbytes == 0) {
              // connection closed (only do debugging output, handling is done further down)
              //GET_TIME
              //fprintf(stderr, "%s %d - INFO - Socket %d hung up.\n", s_error, mypid, i);
            } else {
              GET_TIME
              fprintf(stderr, "%s %d - WARN - Receive error on socket %d.\n", s_error, mypid, i);
            }
            if (i == r_fd) {
              GET_TIME
              fprintf(stderr, "%s %d - ERROR - Rserve socket closed, exiting...\n", s_error, mypid);
              clean_exit();
            }
            close(i);               // bye socket! Close the socket.
            FD_CLR(i, &master);     // Remove from master set
            FD_CLR(i, &client_fds); // Remove from client set
            --nb_clients;
            //GET_TIME
            //fprintf(stderr, "%s %d - INFO - Have %d client(s).\n", s_error, mypid, nb_clients);
            // Remove Rserve socket to read set if we have no more clients
            if (nb_clients == 0) { FD_CLR(r_fd, &master); }
          } else if (i == r_fd) {
            //
            // Rserve data is sent to all clients
            for( j = 0; j <= fdmax; j++) {
              if (FD_ISSET(j, &client_fds)) {
                int total;
                tt_bytes += nbytes;
                total = send(j, buf, nbytes, 0);    // send data
                if (total == -1) {
                  GET_TIME
                  fprintf(stderr, "%s %d - WARN - Send error on socket %d, skipping...\n", s_error, mypid, j);
                  continue;       // Skip this socket send(), continue with others, 'j' loop.
                }
                // Handle incomplete sends
                while (total < nbytes) {
                  int bytesleft = nbytes - total;
                  fprintf(stderr, "%s %d - WARN - Incomplete send - bytes sent: %d, left: %d\n", s_error, mypid, total, bytesleft);
                  int n = send(j, buf+total, bytesleft, 0);
                  if (n == -1) {
                    GET_TIME
                    fprintf(stderr, "%s %d - WARN - Send error on socket %d., skipping...\n", s_error, mypid, j);
                    n = 0;        // Reset size to 0
                    break;
                  }
                  total = total + n;
                }
              }
            } // END: Rserve data to all clients
          } else {
            //
            // Client data is sent only to Rserve
            int total;
            tt_bytes += nbytes;
            total = send(r_fd, buf, nbytes, 0);     // send data
            if (total == -1) {
              GET_TIME
              fprintf(stderr, "%s %d - WARN - Send error on Rserve socket %d, skipping...\n", s_error, mypid, r_fd);
              continue;         // Skip socket send(), continue with others, 'i' loop
            }
            // Handle incomplete sends
            while (total < nbytes) {
              int bytesleft = nbytes - total;
              fprintf(stderr, "%s %d - WARN - Incomplete send - bytes sent: %d, left: %d\n", s_error, mypid, total, bytesleft);
              int n = send(r_fd, buf+total, bytesleft, 0);
              if (n == -1) {
                GET_TIME
                fprintf(stderr, "%s %d - WARN - Send error on Rserve socket %d, skipping...\n", s_error, mypid, r_fd);
                n = 0;         // Reset size to 0
                break;
              }
              total = total + n;
            }
          } // END else: Client data to Rserve
        } // END else: Get data from a client and handle disconnects
      } // END if: New incoming data
    } // END for: Run through the existing connections looking for data to read
  } // END for(;;)--and you thought it would never end!

GET_TIME
fprintf(stderr, "%s %d - INFO - Exiting...\n", s_error, mypid);
clean_exit();
return 0;
}

/*******************************************************************************
 * clean_exit(): Do a clean exit (at least unlink() the socket file)
 ******************************************************************************/
void clean_exit() {
  GET_TIME
  fprintf(stderr, "%s %d - INFO - Statistics - Total clients: %d - Current clients: %d - Alive: %ju sec - Total bytes: %lu\n", s_error, mypid, tt_clients, nb_clients, (e_time - s_time), tt_bytes);
  unlink(s_name);
  exit(0);
}
