<?php
//========== FastRWeb - compatible requests - sample use of the client to behave like Rcgi in FastRWeb
// if(!isset($_SESSION)) session_start();
require_once $_SERVER["DOCUMENT_ROOT"].'/../conf/FastRWeb_config.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/../tools/include.php';
require RSERVE_PHPFW.'/Connection.php';

$user_id = -1;

if (!isset($_SERVER['PHP_AUTH_USER'])) {
    header('WWW-Authenticate: Basic realm="My Realm"');
    header('HTTP/1.0 401 Unauthorized');
    exit;
} else {
	$login = $_SERVER['PHP_AUTH_USER'];
	$code = $_SERVER['PHP_AUTH_PW'];
	$user = $GLOBALS['DB']->get("users",array('user_id','is_active','is_admin'),array('AND' => array('login' => $login,'code' => $code)));
	if(!$user) throw new Exception('Invalid username and / or password',501);
	if($user['is_active'] == 'N' && urldecode($req->getPathInfo()) !== '/user/'.base64_encode($login.":".$code)){
	    header('WWW-Authenticate: Basic realm="My Realm"');
	    header('HTTP/1.0 401 Unauthorized');
	    exit;
	}
	$user_id = $user['user_id'];
}



$root = RSERVE_FASTRWEB ; // set to the root of your FastRWeb installation - must be absolute

function process_FastRWeb($user_id) {
  global $root;
  // $req = array_merge($_GET, $_POST);
  $sID = RSERVE_PORT;
  if (!isset($_SERVER['PATH_INFO'])) { echo "No path specified."; return FALSE; }
  $path = $_SERVER['PATH_INFO'];
  // Check if we have a socketID in the path
  $sp_path = explode('/', $path, 3);
  if (preg_match('/\d{16}/', $sp_path[1])) {
    if(count($sp_path) == 2) $sp_path[2] = "";
    $sID = $sp_path[1];
    $path = "/".$sp_path[2];
	// Verify permissions for loading data
	if($path == '/load_data'){
		if(preg_match("/project_(\d+)\/dataset_(\d+)\//",$_GET['file'],$matches)){
			$project_id = $matches[1];
			$dataset_id = $matches[2];
			require __DIR__."/../../htdocs/api/datasets.php";
			if(!check_dataset_permissions($dataset_id,$user_id,1)){
			    header('WWW-Authenticate: Basic realm="My Realm"');
			    header('HTTP/1.0 401 Unauthorized');
			    exit;
			}
		}
		else{
			error_log('path unvalid');
		    header('WWW-Authenticate: Basic realm="My Realm"');
		    header('HTTP/1.0 401 Unauthorized');
		    exit;

		}
	}
  } elseif (preg_match('/newskt/', $sp_path[1])) {
    // Client kindly asks for a new socket
    try {
      $s = new Rserve_Connection(RSERVE_HOST, RSERVE_PORT);
    } catch (Exception $e) {
      // Error while creating a new Rserve connection
      header("HTTP/1.1 500 Error Unable to create Rserve connection");
      echo "<b>Unable to create new Rserve connection.</b>\n";
      return FALSE;
    }
    // Output the new Rserve socket ID
  	header("Content-type: text/plain");
    echo $s->skt_port."\n";
    return TRUE;
  } else {
    // Only allow 'R.php' with a specified socket ID
    header("HTTP/1.1 500 Error No Rserve socketID");
    echo "<b>No Rserve socketID specified.</b>\n";
    return FALSE;
  }

  // Open a connection to our Rserve socket (or socketeer proxy socket)
  // Even if no R-script path was specified, we create a connection to revive our socket timer
  try {
    $s = new Rserve_Connection(RSERVE_HOST, $sID);
  } catch (Exception $e) {
    header("HTTP/1.1 500 Error Rserve socketID time-out");
    echo $e->getMessage(), "\n";
    return FALSE;
  }
  $sp = str_replace("..", "_", $path); // sanitize paths
  $script = "$root/web.R$sp.R";
  if($sp == '/'){
    echo "keep alive $sID\n";
    return FALSE;
  }
  elseif (!file_exists($script)) {
    header("HTTP/1.1 500 Error Script does not exist");
    echo "<b>Error script '$sp.R' does not exist.</b>\n";
    return FALSE;
 }
  // escape dangerous characters
  $script = str_replace("\\", "\\\\", $script);
  $script = str_replace("\"", "\\\"", $script);
  $qs = str_replace("\\", "\\\\", $_SERVER['QUERY_STRING']);
  $qs = str_replace("\"", "\\\"", $qs);

  $r = $s->evalString("{ qs<-\"$qs\"; setwd('$root/tmp'); library(FastRWeb); .out<-''; cmd<-'html'; ct<-'text/html'; hdr<-''; pars<-list(); lapply(strsplit(strsplit(qs,\"&\")[[1]],\"=\"),function(x) pars[[x[1]]]<<-x[2]); if(exists('init') && is.function(init)) init(); as.character(try({source(\"$script\"); as.WebResult(do.call(run, pars)) },silent=TRUE))}", Rserve_Connection::PARSER_NATIVE);
//    Rserve_close($s);

  if (!is_array($r)) { // this ususally means that an erro rocurred since the returned value is jsut a string
    ob_end_flush();
    echo $r;
    exit(0);
  }

  if (isset($r[2])) header("Content-type: $r[2]");

  if (($r[0] == "file") or ($r[0] == "tmpfile")) {
    if ($r[0] == "tmpfile") { $r[1] = $root."tmp/".$r[1]; }
    $f = fopen($r[1], "rb");
    if ($f === FALSE) {
      header("HTTP/1.1 500 File not found");
      echo "<b>Error File not found.</b>\n";
      exit(1);
    }
    $contents = '';
    while (!feof($f)) $contents .= fread($f, 8192);
    fclose($f);
    ob_end_clean();
    echo $contents;
    if ($r[0] == "tmpfile") unlink($r[1]);
    exit(0);
  }

  if ($r[0] == "html") {
    ob_end_clean();
    echo (is_array($r[1]) ? implode("\n", $r[1]) : $r[1]);
    exit(0);
  }

  print_r($r);

  ob_end_flush();

  exit(0);
}

//--- uncomment the following line if you want this script to serve as FastRWeb handler (see FastRWeb package and IASC paper)
process_FastRWeb($user_id);

?>
